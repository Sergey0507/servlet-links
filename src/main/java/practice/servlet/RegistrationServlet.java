package practice.servlet;

import org.hibernate.Session;
import org.hibernate.query.Query;
import practice.hiber.HibernateUtil;
import practice.hiber.entity.UserEntity;
import practice.servlet.dao.UserDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static practice.servlet.Constant.AUTHORIZED_LABEL;
import static practice.servlet.Constant.AUTHORIZED_USER;

public class RegistrationServlet extends HttpServlet {

   private final HibernateUtil hibernateUtil = HibernateUtil.getInstance();
    private final UserDAO userDao = new UserDAO();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/registration.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        if(req.getAttribute("registration-error") != null) {
            doGet(req, resp);
            return;
        }

        for (Map.Entry <String, String[]> entry : req.getParameterMap().entrySet() ){
            System.out.println(entry.getKey() + " = " + Arrays.toString(entry.getValue()));
        }

       try (Session session = hibernateUtil.getSession()){

            String login = req.getParameterValues("login")[0];
            String password = req.getParameterValues("password")[0];
            String firstName = req.getParameterValues("firstName")[0];
            String lastName = req.getParameterValues("lastName")[0];

            if (login.length() <= 3 || password.length() <= 3){
                req.setAttribute("registration-error", "Короткий логин или пароль");
                getServletContext().getRequestDispatcher("/reg").forward(req, resp);
//            req.getRequestDispatcher("/").forward(req, resp);
                return;
            }

//            Query<UserEntity> query = session.createQuery("SELECT u FROM UserEntity u WHERE u.email = :email", UserEntity.class);
//            query.setParameter("email", login);
//            List<UserEntity> list = query.getResultList();

            UserEntity user = this.userDao.findByEmail(login);

            if (user != null ){
                req.setAttribute("registration-error", "Такой email уже скществует");
                getServletContext().getRequestDispatcher("/reg").forward(req, resp);
//            req.getRequestDispatcher("/").forward(req, resp);
                return;
            }

            UserEntity dbUser = new UserEntity(login, password, firstName, lastName);
            session.save(dbUser);

//            req.getSession().setAttribute(AUTHORIZED_USER, dbUser);
//            req.getSession().setAttribute(AUTHORIZED_LABEL, true);
//       req.getRequestDispatcher("/myjsp").forward(req, resp);
            resp.sendRedirect(req.getContextPath() + "/auth");

        }
    }

    @Override
    public void init() throws ServletException {
        super.init();
    }

}
