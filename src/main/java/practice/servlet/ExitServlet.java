package practice.servlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static practice.servlet.Constant.AUTHORIZED_LABEL;

public class ExitServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

            req.getSession().setAttribute(AUTHORIZED_LABEL, false);
//          req.getRequestDispatcher("/myjsp").forward(req, resp);
            resp.sendRedirect(req.getContextPath() + "/");

    }
}
