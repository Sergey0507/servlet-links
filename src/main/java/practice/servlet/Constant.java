package practice.servlet;

public class Constant {

    public static final String AUTHORIZED_LABEL = "userAuthorized";
    public static final String AUTHORIZED_USER = "user";

    private Constant (){}
}
