package practice.servlet;

import org.hibernate.Session;
import org.hibernate.query.Query;
import practice.hiber.HibernateUtil;
import practice.hiber.entity.NoteEntity;
import practice.hiber.entity.UserEntity;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static practice.servlet.Constant.AUTHORIZED_LABEL;
import static practice.servlet.Constant.AUTHORIZED_USER;

public class NotesServlet extends HttpServlet {

    private final HibernateUtil hibernateUtil = HibernateUtil.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try (Session session = hibernateUtil.getSession()){

            UserEntity userEntity = (UserEntity) req.getSession().getAttribute(AUTHORIZED_USER);

            Query<NoteEntity> query = session.createQuery("SELECT n FROM NoteEntity n WHERE n.userId = :user", NoteEntity.class);

            query.setParameter("user", userEntity);

//            UserEntity dbUser = query.getSingleResult();

            List<NoteEntity> list = query.getResultList();

            for (NoteEntity n : list) {
                System.out.println(n.toString());
            }
            req.setAttribute("notes", list);
        }
        req.getRequestDispatcher("/notes.jsp").forward(req, resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        System.out.println("DELET");

        try (Session session = hibernateUtil.getSession()) {

            UserEntity userEntity = (UserEntity) req.getSession().getAttribute(AUTHORIZED_USER);

            Query<NoteEntity> query = session.createQuery("DELETE FROM NoteEntity n WHERE n.userId = :user", NoteEntity.class);

            query.setParameter("user", userEntity);

            session.clear();
            session.flush();

        }
//        req.getRequestDispatcher("/notes.jsp").forward(req, resp);
    }


    @Override
    public void init () throws ServletException {
        super.init();
    }


}
