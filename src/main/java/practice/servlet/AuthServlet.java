package practice.servlet;

import org.hibernate.Session;
import org.hibernate.query.Query;
import practice.hiber.HibernateUtil;
import practice.hiber.entity.UserEntity;
import practice.servlet.dao.UserDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import static practice.servlet.Constant.AUTHORIZED_LABEL;
import static practice.servlet.Constant.AUTHORIZED_USER;

public class AuthServlet extends HttpServlet {

//    private final HibernateUtil hibernateUtil = HibernateUtil.getInstance();
    private final UserDAO userDao = new UserDAO();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/auth.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        if(req.getAttribute("auth-error") != null) {
            doGet(req, resp);
            return;
        }

        for (Map.Entry <String, String[]> entry : req.getParameterMap().entrySet() ){
            System.out.println(entry.getKey() + " = " + Arrays.toString(entry.getValue()));
        }

//        try (Session session = hibernateUtil.getSession()){

            String login = req.getParameterValues("login")[0];
            String password = req.getParameterValues("password")[0];

//            Query<UserEntity> query = session.createQuery("SELECT u FROM UserEntity u WHERE u.email = :email", UserEntity.class);
//            query.setParameter("email", login);

//            UserEntity dbUser = query.getSingleResult();
//            List<UserEntity> list = query.getResultList();

        UserEntity dbUser = this.userDao.findByEmail(login);

//            UserEntity dbUser;
//            if (list.size() > 0 ){
//                dbUser = list.get(0);
//            }else {
//                dbUser = null;
//            }

            if (dbUser == null || ! dbUser.getPassword().equals(password)){
                req.setAttribute("auth-error", "Login or password incorrect");
                getServletContext().getRequestDispatcher("/auth").forward(req, resp);
//            req.getRequestDispatcher("/").forward(req, resp);
                return;
            }

            req.getSession().setAttribute(AUTHORIZED_USER, dbUser);
            req.getSession().setAttribute(AUTHORIZED_LABEL, true);
//       req.getRequestDispatcher("/myjsp").forward(req, resp);
            resp.sendRedirect(req.getContextPath() + "/");

        }

//    }

    @Override
    public void init() throws ServletException {
        super.init();
    }
}
