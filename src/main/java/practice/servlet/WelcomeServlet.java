package practice.servlet;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

public class WelcomeServlet extends HttpServlet {

    @Override
    public void init() {
        System.out.println("WelcomeServlet -> Init");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        Enumeration<String> headName = req.getHeaderNames();
        System.out.println();
        while (headName.hasMoreElements()){

            String headerName = headName.nextElement();
            System.out.println(String.format("%s = %s", headerName, req.getHeader(headerName)));
        }
        System.out.println();
        System.out.println("WelcomeServlet -> GET");
        resp.getWriter().write("<h1>Welcome Servlet GET</h1>");
    }

    @Override
    public void destroy() {
        System.out.println("WelcomeServlet -> Destroy");
    }
}
