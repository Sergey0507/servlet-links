package practice.servlet.dao;

import org.hibernate.Session;
import org.hibernate.query.Query;
import practice.hiber.HibernateUtil;
import practice.hiber.entity.UserEntity;

import java.util.List;

public class UserDAO  {

    private final HibernateUtil hibernateUtil = HibernateUtil.getInstance();

    public UserEntity findByEmail(String email){

        try (Session session = this.hibernateUtil.getSession()) {

            Query<UserEntity> query = session.createQuery("SELECT u FROM UserEntity u WHERE u.email = :email", UserEntity.class);

            query.setParameter("email", email);

            List<UserEntity> list = query.getResultList();

            return list.size() > 0 ? list.get(0) : null;

        }
    }
}
