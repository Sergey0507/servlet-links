package practice.servlet;

import org.hibernate.Session;
import practice.hiber.HibernateUtil;
import practice.hiber.entity.NoteEntity;
import practice.hiber.entity.UserEntity;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static practice.servlet.Constant.AUTHORIZED_LABEL;
import static practice.servlet.Constant.AUTHORIZED_USER;

public class NoteAddServlet extends HttpServlet {

    private final HibernateUtil hibernateUtil = HibernateUtil.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/noteAdd.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        if(req.getAttribute("note-error") != null) {
            doGet(req, resp);
            return;
        }

        for (Map.Entry <String, String[]> entry : req.getParameterMap().entrySet() ){
            System.out.println(entry.getKey() + " = " + Arrays.toString(entry.getValue()));
        }

        try (Session session = hibernateUtil.getSession()){

            String title = req.getParameterValues("title")[0];
            String txt = req.getParameterValues("txt")[0];

            if (title.length() == 0){
                req.setAttribute("note-error", "Введите название заметки");
                getServletContext().getRequestDispatcher("/note/add").forward(req, resp);
//            req.getRequestDispatcher("/").forward(req, resp);
                return;
            }

            if (txt.length() == 0){
                req.setAttribute("note-error", "Сделайте заметку");
                getServletContext().getRequestDispatcher("/note/add").forward(req, resp);
//            req.getRequestDispatcher("/").forward(req, resp);
                return;
            }

            UserEntity userEntity = (UserEntity) req.getSession().getAttribute(AUTHORIZED_USER);

            NoteEntity noteEntity = new NoteEntity(title, txt, userEntity);
            session.save(noteEntity);

            resp.sendRedirect(req.getContextPath() + "/note");

        }
    }

    @Override
    public void init() throws ServletException {
        super.init();
    }

}
