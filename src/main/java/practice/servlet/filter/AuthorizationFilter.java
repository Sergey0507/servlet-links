package practice.servlet.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static practice.servlet.Constant.AUTHORIZED_LABEL;

public class AuthorizationFilter implements Filter {

    private static final List<String> PUBLIC_URLS = new ArrayList<>();
    private static final List<String> PRIVATE_URLS = new ArrayList<>();
    private static final List<String> ANONYMOUS_URLS = new ArrayList<>();

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("SecondFilter-> Init");
        PUBLIC_URLS.add("/welcome");
        PUBLIC_URLS.add("/");
        PUBLIC_URLS.add("/reg");
        PRIVATE_URLS.add("/myjsp");
        PRIVATE_URLS.add("/hello");
        PRIVATE_URLS.add("/note/add");
        PRIVATE_URLS.add("/note");
        PRIVATE_URLS.add("/exit");
        ANONYMOUS_URLS.add("/auth");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        System.out.println("SecondFilter-> doFilter");

        HttpServletRequest req = ((HttpServletRequest) request);
        HttpServletResponse resp = ((HttpServletResponse) response);

        Boolean userAuthorized = (Boolean) req.getSession().getAttribute(AUTHORIZED_LABEL);

        if(userAuthorized == null) {
            userAuthorized = false;
        }

        String currentURI = req.getRequestURI().replaceAll(req.getServletContext().getContextPath(), "");

        if(PUBLIC_URLS.contains(currentURI)) {
            filterChain.doFilter(request, response);
        } else if(PRIVATE_URLS.contains(currentURI)) {
            if(userAuthorized) {
                filterChain.doFilter(request, response);
            } else {
                resp.sendRedirect(req.getContextPath() + "/auth");
            }
        } else if(ANONYMOUS_URLS.contains(currentURI)) {
            if(!userAuthorized) {
                filterChain.doFilter(request, response);
            } else {
                resp.sendRedirect(req.getContextPath() + "/");
            }
        } else {
            resp.sendError(404);
        }

       /* switch (currentURI) {
            case "/":
                if(!userAuthorized) {
                    filterChain.doFilter(request, response);
                } else {
                    resp.sendRedirect("/myjsp");
                }
                break;
            case "/myjsp":
                if(userAuthorized) {
                    filterChain.doFilter(request, response);
                } else {
                    resp.sendRedirect("/");
                }
                break;
        }

        System.out.println();

        filterChain.doFilter(request, response);*/
    }

    @Override
    public void destroy() {
        System.out.println("SecondFilter-> Destroy");
    }
}
