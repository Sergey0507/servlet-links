package practice.hiber;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class HibernateUtil {

    private SessionFactory sessionFactory;
    private StandardServiceRegistry standardServiceRegistry;

    private static HibernateUtil instance;

    private HibernateUtil() {

        try {

            StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();

            this.standardServiceRegistry = registryBuilder.configure().build();

            MetadataSources metadataSources = new MetadataSources(this.standardServiceRegistry);

            //metadataSources.addPackage("info.tsyklop.classwork.orm.entity");

//            metadataSources.addAnnotatedClass(UserEntity.class);
//            metadataSources.addAnnotatedClass(ItemEntity.class);
//            metadataSources.addAnnotatedClass(OrderEntity.class);

            this.sessionFactory = metadataSources.getMetadataBuilder().build().getSessionFactoryBuilder().build();

        } catch (Exception e) {
            if (this.standardServiceRegistry != null) {
                StandardServiceRegistryBuilder.destroy(this.standardServiceRegistry);
            }
            throw new HibernateException(e);
        }

    }

    public static HibernateUtil getInstance() {
        HibernateUtil localInstance = instance;
        if (localInstance == null) {
            synchronized (HibernateUtil.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new HibernateUtil();
                }
            }
        }
        return localInstance;
    }

    public Session getSession() {
        return sessionFactory.openSession();
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void shutdown() {
        if (this.standardServiceRegistry != null) {
            StandardServiceRegistryBuilder.destroy(this.standardServiceRegistry);
        }
    }
}

