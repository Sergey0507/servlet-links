package practice.hiber.entity;

import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.time.LocalDateTime;

@MappedSuperclass
public abstract class AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ColumnDefault("CURRENT_TIMESTAMP(2)")
    @Column(name = "updated", nullable = false)
    private LocalDateTime updated = LocalDateTime.now();

    @ColumnDefault("CURRENT_TIMESTAMP(2)")
    @Column(name = "created", unique = false, nullable = false)
    private LocalDateTime created = LocalDateTime.now();

    @PrePersist
    public void prePersist() {

    }

    @PreUpdate
    private void PreUpdate (){this.updated = LocalDateTime.now();}

    public long getId() {
        return id;
    }

    public LocalDateTime getUpdated() {
        return updated;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    @Override
    public String toString() {
        return "AbstractEntity{" +
                "id=" + id +
                ", updated=" + updated +
                ", created=" + created +
                '}';
    }
}
