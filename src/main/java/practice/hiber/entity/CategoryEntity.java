package practice.hiber.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name = "category")
@Entity
public class CategoryEntity extends AbstractEntity {

    @Column(name = "name", nullable = false)
    private String name;

    public CategoryEntity (){

    }

    public CategoryEntity(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "CategoryEntity{" +
                "name='" + name + '\'' +
                '}';


    }
}
