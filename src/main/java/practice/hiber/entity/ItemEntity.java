package practice.hiber.entity;


import net.bytebuddy.implementation.bind.annotation.Default;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@Table(name = "item")
@Entity
public class ItemEntity extends AbstractEntity {

    @Column(name = "name", nullable = false)
    private String name;

    @ColumnDefault("0.0")
    @Column(name = "price")
    private Double price;

   @Column(name = "image")
   private String image;

   @Type(type = "text")
   @Column(name = "description")
   private String description;

   @ManyToOne(targetEntity = CategoryEntity.class, optional = false, fetch = FetchType.LAZY)
   @JoinColumn(name = "categoryId")
   private CategoryEntity categoryId;

   private ItemEntity(){
   this.categoryId = null;
   }

    public ItemEntity(String name, Double price, String image, String description, CategoryEntity categoryId) {
        this.name = name;
        this.price = price;
        this.image = image;
        this.description = description;
        this.categoryId = categoryId;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CategoryEntity getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(CategoryEntity categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public String toString() {
        return "ItemEntity{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", image='" + image + '\'' +
                ", description='" + description + '\'' +
                ", categoryId=" + categoryId +
                '}';
    }
}
