package practice.hiber.entity;

        import org.hibernate.annotations.Type;

        import javax.persistence.*;
        import java.util.HashSet;
        import java.util.Set;

@Table(name = "order1")
@Entity
public class OrderEntity extends AbstractEntity {

    @ManyToOne(targetEntity = UserEntity.class, optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "userID")
    private UserEntity userID;


    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(
            name = "ord_item",
            joinColumns = @JoinColumn(name = "order1_id"),
            inverseJoinColumns = @JoinColumn(name = "item_id"))
    private Set<ItemEntity> item = new HashSet<>();


    public OrderEntity() {

    }

    public OrderEntity(UserEntity userID) {
        this.userID = userID;
    }

    public OrderEntity(UserEntity userID, Set<ItemEntity> item) {
        this.userID = userID;
        this.item = item;
    }

    public UserEntity getUserID() {
        return userID;
    }

    public Set<ItemEntity> getItem() {
        return item;
    }

    @Override
    public String toString() {
        return "OrderEntity{" +
                "userID=" + userID +
                ", item=" + item +
                '}';
    }
}
