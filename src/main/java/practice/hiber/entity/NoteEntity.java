package practice.hiber.entity;

        import org.hibernate.annotations.Type;

        import javax.persistence.*;

@Table(name = "note")
@Entity
public class NoteEntity extends AbstractEntity {

    @Column(name = "title", nullable = false)
    private String title;

    @Type(type = "text")
    @Column(name = "txt", nullable = false)
    private String txt;

    @ManyToOne(targetEntity = UserEntity.class, optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "userId")
    private UserEntity userId;


    public NoteEntity(){}

    public NoteEntity(String title, String txt, UserEntity userId) {
        this.title = title;
        this.txt = txt;
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTxt() {
        return txt;
    }

    public void setTxt(String txt) {
        this.txt = txt;
    }

    @Override
    public String toString() {
        return "NoteEntity{" +
                "title='" + title + '\'' +
                ", txt='" + txt + '\'' +
                '}';
    }
}
